package listacontato;

public class Principal {

	public static void main(String[] args) {
		//Cria��o de um objeto da classe Lista
		Lista listasequencial = new Lista();
		
		//Caso retorne true, significa que o n � igual a zero e a lista est� vazia.
		System.out.println("A lista est� vazia? "+listasequencial.isVazia());
		
		//Caso retorne true, significa que a lista esta cheia, do contr�rio, ainda podemos inserir elementos nela
		System.out.println("A lista est� cheia? "+listasequencial.isCheia());
		
		//Retorna o tamanho atual da lista
		System.out.println("Tamanho da lista: "+listasequencial.tamanho());
		
		//Criando 4 objetos
		Contato c1 = new Contato();
		c1.setNome("Goku");
		c1.setTelefone("98888-0000");
		Contato c2 = new Contato();
		c2.setNome("Gohan");
		c2.setTelefone("97777-0001");
		Contato c3 = new Contato();
		c3.setNome("Goten");
		c3.setTelefone("96666-0002");
		Contato c4 = new Contato();
		c4.setNome("Bulma");
		c4.setTelefone("95555-0003");
		
		//Inserindo na lista
		listasequencial.inserir(0, c1);
		listasequencial.inserir(1, c2);
		listasequencial.inserir(2, c3);
		listasequencial.inserir(3, c4);
		listasequencial.inserir(2, c4);
		
		//Removendo da lista
		listasequencial.remover(0);
		
		//Buscando Elemento por �ndice
		System.out.println("Buscar elemento 2: "+listasequencial.buscar(2).getNome());
		
		//Buscar Posi��o do elemento c2
		System.out.println("Posi��o do elemento Gohan: "+listasequencial.getPosicao(c2));
		
		System.out.println("A lista est� vazia? "+listasequencial.isVazia());
		
		listasequencial.exibirLista();
		
	}

}
