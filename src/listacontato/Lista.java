package listacontato;

public class Lista {
	
	Contato[] contatos = new Contato [100]; //Vetor que armazena os elementos
	int n = 0; //Posi��o (ind�ce) do ultimo elemento da lista
	
	/*Verifica se a lista esta vazia ou n�o.
	 * @return true se a lista estiver vazia, ou false caso contr�io
	 */
	public boolean isVazia(){
		return(n==0);
	}
	
	/*Verifica se a lista esta cheia ou n�o.
	 * @return true se a lista estiver cheia, ou false caso contr�rio
	 */
	public boolean isCheia(){
		return (n== contatos.length);
	}
	
	/*Obt�m o tamanho atual da lista, n�o o tamanho m�ximo.
	 * @return a quantidade de elementos que est�o na lista.
	 */
	public int tamanho(){
		return n;
	}
	
	//Obt�m um elemento da lista espec�fico por posi��o.
	public Contato buscar(int pos){
		//Verifica se a vari�vel pos � v�lida.
		if ((pos>= n)||(pos < 0)){return null;}
		
		return contatos[pos];
	}
	
	/* Recebe dois contatos e vai comparar as vari�veis de inst�ncia deles por meio
	do m�todo equals. Atente que existem duas compara��es. Primeiro s�o
	comparados os nomes da cada contato e depois os telefones. Caso as duas
	condi��es sejam verdadeiras, o m�todo ir� retornar verdadeiro. */
	public boolean compara(Contato contato1, Contato contato2){
		return (contato1.getNome().equals(contato2.getNome()))&&(contato1.getTelefone().equals(contato2.getTelefone()));
	}
	public int getPosicao(Contato contato){
		for(int i=0; i < n; i++)
			if(compara(contatos[i],contato))
				return i;
		return -1;
	}
	public boolean compara(Contato contato1, Contato contato2){
		return (contato1.getNome().equals(contato2.getNome()))&&(contato1.getTelefone().equals(contato2.getTelefone()));
	}
	
	/* Desloca os elementos da lista para a direita a partir de pos
	* @param pos posi��o do elemento, a partir do qual ser� iniciado o
	deslocamento.
	*/
	public void deslocaDireita(int pos){
		for(int i = n; i > pos; i--)
			contatos[i] = contatos[i-1];
	}
	
	/* Insere um elemento (dado) em determinada posi��o da lista.
	* @param pos posi��o do elemento, a partir do qual ser� iniciado o deslocamento.
	* @param dado elemento a ser inserido
	* @return false se a posi��o for inv�lida ou se a lista estiver cheia, caso contrario, retorna true. */
	boolean inserir (int pos, Contato contato){
		if (isCheia()||(pos>n)||(pos<0))
			return false;
		
		deslocaDireita(pos);
		contatos[pos] = contato;
		n++;
		return true;
	}
	
	// Desloca os elementos da lista para a esquerda a partir de pos.
	// @param pos posi��o do elemento, a partir do qual ser� iniciado o
	// deslocamento.
	public void deslocaEsquerda(int pos){
		for(int i = pos; i < n-1; i++)
			contatos[i] = contatos[i+1];
	}
	
	public boolean remover (int pos){
		if (pos >= n || pos < 0)
			return false;
		
		deslocaEsquerda(pos);
		n--;
		return true;
	}
	
	public void exibirLista(){
		for (int i=0; i<n; i++){
			System.out.println("\nContato "+(i+1)+"\nNome: "
			+ contatos[i].getNome()
			+"\nTelefone: "
			+ contatos[i].getTelefone());
		}
	}
}
